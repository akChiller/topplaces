//
//  FlickrTopPlacesTVC.m
//  TopPlaces
//
//  Created by Colin Reisterer on 11/25/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import "FlickrTopPlacesTVC.h"
#import "FlickrFetcher.h"
#import "FlickrPlaceTVC.h"

@interface FlickrTopPlacesTVC ()

@property (strong, nonatomic) NSMutableDictionary *countryDictionary;
@property (strong, nonatomic) NSArray *countryNames;

@end

@implementation FlickrTopPlacesTVC

#pragma mark - Property Overrides

- (void)setTopPlaces:(NSArray *)topPlaces
{
    _topPlaces = topPlaces;
    // populate countries array
    [self setPlacesByCountry];
    [self.tableView reloadData];
}

- (NSMutableDictionary *)countryDictionary
{
    if (!_countryDictionary) {
        _countryDictionary = [NSMutableDictionary new];
    }
    return _countryDictionary;
}
#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchPlaces];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchPlaces {
    NSURL *url = [FlickrFetcher URLforTopPlaces];
    dispatch_queue_t fetchQ = dispatch_queue_create("fetchPlaces queue", NULL);
    dispatch_async(fetchQ, ^{
        NSData *jsonResults = [NSData dataWithContentsOfURL:url];
        NSDictionary *propertyListResults = [NSJSONSerialization JSONObjectWithData:jsonResults options:0 error:NULL];
        // set topPlaces back on main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            self.topPlaces = [propertyListResults valueForKeyPath:@"places.place"];
        });
    });
    
}

- (void)setPlacesByCountry {
//    NSLog(@"%@", self.topPlaces);
    [self.topPlaces enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *place = (NSDictionary *)obj;
            NSString *placeName = place[FLICKR_PLACE_NAME];
            
            // split placeName to get country only
            NSMutableArray *placeNameSeparated = [placeName componentsSeparatedByString:@","].mutableCopy;
            
            // get substrings of name and strip any leading/trailing whitespace
            NSString *placeDetailName = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:[placeNameSeparated firstObject]];
            [placeNameSeparated removeObjectAtIndex:0];
            NSString *placeCountryName = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:[placeNameSeparated lastObject]];
            [placeNameSeparated removeLastObject];
            NSString *placeRemainingName = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:[placeNameSeparated componentsJoinedByString:@","]];
            
            // add place substrings as place dictionary entries
            NSMutableDictionary *mutablePlace = place.mutableCopy;
            [mutablePlace addEntriesFromDictionary:@{@"detailName": placeDetailName, @"countryName": placeCountryName, @"remainingName": placeRemainingName}];
            
            // append to country array in dict if it exists, otherwise create
            if ([self.countryDictionary[placeCountryName] isKindOfClass:[NSMutableArray class]]) {
                NSMutableArray *placesArray = (NSMutableArray *)self.countryDictionary[placeCountryName];
                [placesArray addObject:mutablePlace];
            } else {
                self.countryDictionary[placeCountryName] = [NSMutableArray arrayWithObject:mutablePlace];
            }
        }
    }];
    
    // sort the places in each country
    [self.countryDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([obj isKindOfClass:[NSMutableArray class]]) {  // places are stored in mutable array
            // sort the mutable array
            NSMutableArray *placesArray = (NSMutableArray *)obj;
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:FLICKR_PLACE_NAME ascending:YES];
            [placesArray sortUsingDescriptors:@[sort]];
        }
    }];
    
    // create a sorted array of country names
    self.countryNames = [[self.countryDictionary allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return (int)[self.countryNames count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSString *countryName = [self.countryNames objectAtIndex:(NSUInteger)section];
    return (int)[self.countryDictionary[countryName] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Flickr Place Cell" forIndexPath:indexPath];
    
    NSString *countryName = [self.countryNames objectAtIndex:(NSUInteger)indexPath.section];
    NSDictionary *place = [self.countryDictionary[countryName] objectAtIndex:(NSUInteger)indexPath.row];
    cell.textLabel.text = place[@"detailName"];
    cell.detailTextLabel.text = place[@"remainingName"];
    
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.countryNames objectAtIndex:(NSUInteger)section];
}

#pragma mark - Helper methods

// Trim the input string by removing leading and trailing white spaces
// and return the result
- (NSString *)stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:(NSString *)string
{
    NSString *leadingTrailingWhiteSpacesPattern = @"(?:^\\s+)|(?:\\s+$)";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:leadingTrailingWhiteSpacesPattern options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSRange stringRange = NSMakeRange(0, string.length);
    NSString *trimmedString = [regex stringByReplacingMatchesInString:string options:NSMatchingReportProgress range:stringRange withTemplate:@"$1"];
    
    return trimmedString;
}

- (void)prepareFlickrPlaceTVC:(FlickrPlaceTVC *)tvc toDisplayPlace:(NSDictionary *)place
{
    tvc.place = place;
    tvc.title = place[@"detailName"];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        // get indexPath to determine place
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            NSString *countryName = [self.countryNames objectAtIndex:(NSUInteger)indexPath.section];
            NSDictionary *place = [self.countryDictionary[countryName] objectAtIndex:(NSUInteger)indexPath.row];
            if ([segue.destinationViewController isKindOfClass:[FlickrPlaceTVC class]]) {
                [self prepareFlickrPlaceTVC:(FlickrPlaceTVC *)segue.destinationViewController toDisplayPlace:place];
            }
        }
    }
}


@end
