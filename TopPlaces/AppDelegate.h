//
//  AppDelegate.h
//  TopPlaces
//
//  Created by Colin Reisterer on 11/25/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

