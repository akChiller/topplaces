//
//  FlickrRecentPhotosTVC.h
//  TopPlaces
//
//  Created by Colin Reisterer on 12/6/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrPhotosTVC.h"

@interface FlickrRecentPhotosTVC : FlickrPhotosTVC

@end
