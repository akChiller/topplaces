//
//  ImageViewController.m
//  ShutterBug
//
//  Created by Colin Reisterer on 11/17/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController () <UIScrollViewDelegate, UISplitViewControllerDelegate>

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImage *image;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;


@end

@implementation ImageViewController

- (void)setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
    _scrollView.maximumZoomScale = 2.0;
    _scrollView.delegate = self;
    self.scrollView.zoomScale = 1.0;
    self.scrollView.contentSize = self.image ? self.image.size : CGSizeZero;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.scrollView addSubview:self.imageView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self zoomScrollViewToFitImage];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self zoomScrollViewToFitImage];
    [self centerContentInScrollView];
}

- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    [self startDownloadingImage];
}


- (void)startDownloadingImage
{
    self.image = nil;
    [self.loadingIndicator startAnimating];
    if (self.imageURL) {
        NSURLRequest *request = [NSURLRequest requestWithURL:self.imageURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        // Improvement: check if a download is ongoing already
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];  // callback off main queue
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request
            completionHandler:^(NSURL *localFile, NSURLResponse *response, NSError *error) {
                if (!error) {
                    // ensure user still wants this fetched image
                    if ([request.URL isEqual:self.imageURL]) {
                        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:localFile]];
                        // need to set image on the main thread
                        dispatch_async(dispatch_get_main_queue(), ^{ self.image = image; });
                    }
                }
            }];
        [task resume];
    }
}

- (UIImageView *)imageView
{
    if (!_imageView) _imageView = [[UIImageView alloc] init];
    return _imageView;
}

- (UIImage *)image
{
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image
{
    self.imageView.image = image;
    [self.loadingIndicator stopAnimating];
    self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    self.scrollView.zoomScale = 1.0;
    self.scrollView.contentSize = self.image ? self.image.size : CGSizeZero;
    [self zoomScrollViewToFitImage];
    [self centerContentInScrollView];
}

- (void)zoomScrollViewToFitImage
{
    if (self.image && !CGSizeEqualToSize(self.scrollView.contentSize, CGSizeZero)) {
        self.scrollView.contentOffset = CGPointZero;
        self.scrollView.contentInset = UIEdgeInsetsZero;
        CGFloat scaleWidth = self.scrollView.frame.size.width / self.image.size.width;
        CGFloat scaleHeight = self.scrollView.frame.size.height / self.image.size.height;
        CGFloat minScale = MIN(scaleWidth, scaleHeight);
        self.scrollView.minimumZoomScale = minScale;
        [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:NO];
    }
    
}

- (void)centerContentInScrollView {
    CGFloat top = 0, left = 0;
    if (self.scrollView.contentSize.width < self.scrollView.frame.size.width) {
        left = (self.scrollView.frame.size.width - self.scrollView.contentSize.width) * 0.5f;
    }
    if (self.scrollView.contentSize.height < self.scrollView.frame.size.height) {
        top = (self.scrollView.frame.size.height-self.scrollView.contentSize.height) * 0.5f;
    }
    self.scrollView.contentInset = UIEdgeInsetsMake(top, left, top, left);
}

#pragma mark - UIScrollViewDelegate Methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

#pragma mark - UISplitViewControllerDelegate
- (void)awakeFromNib
{
    self.splitViewController.delegate = self;
}

- (BOOL)splitViewController:(UISplitViewController *)splitViewController shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}

- (void)splitViewController:(UISplitViewController *)splitViewController willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = aViewController.title;
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationItem.leftBarButtonItem = nil;
    
}


@end
