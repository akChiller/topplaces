//
//  FlickrPlaceTVC.m
//  TopPlaces
//
//  Created by Colin Reisterer on 12/3/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import "FlickrPlaceTVC.h"
#import "FlickrFetcher.h"
#import "ImageViewController.h"

@interface FlickrPlaceTVC ()

@end

@implementation FlickrPlaceTVC

#pragma mark - Property Overrides

- (void)setPlace:(NSDictionary *)place
{
    _place = place;
    [self fetchPhotos];
}

- (void)fetchPhotos {
    NSURL *url = [FlickrFetcher URLforPhotosInPlace:self.place[FLICKR_PLACE_ID] maxResults:50];
    dispatch_queue_t fetchQ = dispatch_queue_create("fetchPhotos queue", NULL);
    dispatch_async(fetchQ, ^{
        NSData *jsonResults = [NSData dataWithContentsOfURL:url];
        NSDictionary *propertyListResults = [NSJSONSerialization JSONObjectWithData:jsonResults options:0 error:NULL];
        // set topPlaces back on main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photos = [propertyListResults valueForKeyPath:@"photos.photo"];
        });
    });
    
}

@end
