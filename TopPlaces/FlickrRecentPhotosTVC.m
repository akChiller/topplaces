//
//  FlickrRecentPhotosTVC.m
//  TopPlaces
//
//  Created by Colin Reisterer on 12/6/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import "FlickrRecentPhotosTVC.h"
#import "RecentPhotosManager.h"

@interface FlickrRecentPhotosTVC ()

@end

@implementation FlickrRecentPhotosTVC

#pragma mark - Property Overrides


#pragma mark - View Lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchRecentPhotos];
}

- (void)fetchRecentPhotos
{
    NSArray *recentPhotos = [RecentPhotosManager recentPhotos];
    // sort the photos
    self.photos = [[recentPhotos reverseObjectEnumerator] allObjects];
}

@end
