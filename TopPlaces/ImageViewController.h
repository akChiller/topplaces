//
//  ImageViewController.h
//  ShutterBug
//
//  Created by Colin Reisterer on 11/17/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (nonatomic, strong)NSURL *imageURL;

@end


