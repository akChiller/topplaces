//
//  main.m
//  TopPlaces
//
//  Created by Colin Reisterer on 11/25/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
