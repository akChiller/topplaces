//
//  RecentPhotosManager.h
//  TopPlaces
//
//  Created by Colin Reisterer on 12/6/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecentPhotosManager : NSObject

+ (NSArray *)recentPhotos;  // Fetched from NSUserDefaults;

+ (void)addPhoto:(NSDictionary *)photo;

@end
