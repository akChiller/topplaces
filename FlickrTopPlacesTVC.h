//
//  FlickrTopPlacesTVC.h
//  TopPlaces
//
//  Created by Colin Reisterer on 11/25/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrTopPlacesTVC : UITableViewController

@property (strong, nonatomic) NSArray *topPlaces;
@property (strong, nonatomic) NSArray *countries;

@end
