//
//  RecentPhotosManager.m
//  TopPlaces
//
//  Created by Colin Reisterer on 12/6/14.
//  Copyright (c) 2014 Stanford Class. All rights reserved.
//

#import "RecentPhotosManager.h"
#import "FlickrFetcher.h"

@interface RecentPhotosManager ()

@end

@implementation RecentPhotosManager

const int MAX_RECENT_PHOTOS = 20;

#pragma mark - Property Overrides

+ (NSArray *)recentPhotos
{
    return [[NSUserDefaults standardUserDefaults] arrayForKey:@"recentPhotos"];
}

+ (void)addPhoto:(NSDictionary *)photo
{
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *recentPhotos = [self recentPhotos];
    NSMutableArray *recentPhotosMutable;
    if (recentPhotos) {
        recentPhotosMutable = recentPhotos.mutableCopy;
    } else {
        recentPhotosMutable = [NSMutableArray new];
    }
    for (NSDictionary *savedPhoto in recentPhotos) {
        if ([savedPhoto[FLICKR_PHOTO_ID] isEqualToString:photo[FLICKR_PHOTO_ID]]) {
            [recentPhotosMutable removeObject:savedPhoto];
        }
    }
    [recentPhotosMutable addObject:photo];
    
    // trim the recentPhotos array to max allowed length if necessary
    if (recentPhotosMutable.count > MAX_RECENT_PHOTOS) {
        [recentPhotosMutable removeObjectsInRange:NSMakeRange(0, recentPhotosMutable.count - MAX_RECENT_PHOTOS)];
    }
    [standardDefaults setObject:recentPhotosMutable forKey:@"recentPhotos"];
    [standardDefaults synchronize];
}

@end
